import React from 'react';
import {SafeAreaView, ScrollView, useColorScheme} from 'react-native';
import {Provider} from 'react-redux';

import {Store} from './src/Store';

import Todos from './src/Container';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <Provider store={Store}>
      <SafeAreaView style={backgroundStyle}>
        <ScrollView
          contentInsertAdjustmentBehavior="automatic"
          style={backgroundStyle}>
          <Todos />
        </ScrollView>
      </SafeAreaView>
    </Provider>
  );
};

export default App;

import {FETCH_TODO_SUCCESS} from '../Types';
import axios from 'axios';

export const saveTodos = data => ({
  type: FETCH_TODO_SUCCESS,
  payload: data,
});

// function untuk mendapatkan data Todos
export const getTodos = (state, action) => {
  return async dispatch => {
    const restTodos = await axios.get('http://code.aldipee.com/api/v1/todos');
    if (restTodos.data.results.length > 0) {
      console.log(restTodos.data.results)
      dispatch(saveTodos(restTodos.data.results));
      
    }
  };
};

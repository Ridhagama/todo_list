import {FETCH_TODO_SUCCESS } from '../Types'
const initialState = {
    todos: []
}
const TodoReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TODO_SUCCESS:
            console.log(action.payload)
            return {
                ...state, 
                todos: action.payload,
            }
        default: 
            return state;
    }
}
export default TodoReducer